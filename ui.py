import tkinter as tk


class AbstractLcdNumber:
    _default_params = dict(
        digit_count=1,
        background=None,
        active_color=None,
        inactive_color=None,
        display_size=None,
        dispoay_position=None,
    )
    
    def __init__(self, **kwargs):
        self.check_kwargs(self, kwargs)
        
        actual_params = self._default_params.copy()
        actual_params.update(kwargs)
        
        # Не самый хороший вариант, но пока сойдёт
        for k, v in actual_params.items():
            setattr(self, k, v)
    
    def check_kwargs(self, kwargs):
        kw_keys = set(kwargs.keys())
        default_keys = set(self._default_params.keys())
        
        unexpected_args = kw_keys - default_keys
        
        if unexpected_args:
            raise TypeError(f'Unexpected args {unexpected_args}!')


class TkLcdNumber(AbstractLcdNumber, tk.Canvas):
    pass

